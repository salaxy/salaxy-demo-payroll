import * as chai from "chai";

import { TestHelper, OAuthMessage, OAuthGrantType, AjaxNode, OAuth2, Calculations } from "@salaxy/core";

import { config } from "./config";

describe("02 Authenticated connection to the server.", function () {

    // Test framework setup
    const expect = chai.expect;
    this.timeout(30000); // Test-helper sets the timeout by default.

    it("Authenticate to server without test helper.", () =>{
        const request: OAuthMessage = {
            grant_type: OAuthGrantType.Password,
            username: config.username,
            password: config.password,
          };
          const ajax = new AjaxNode();
          // Hard-coding address for demonstration purposes.
          ajax.serverAddress = "https://test-api.salaxy.com";
          return new OAuth2(ajax).token(request).then((oauthResponse) => {
            ajax.setCurrentToken(oauthResponse.access_token);
            return oauthResponse;
          })
          .then(() => {
              new Calculations(ajax).getAll()
            .then((calcs) => {
                expect(calcs.length).to.be.greaterThan(1);
            });
          });
    })

    it("Authenticate to server using test helper.", async () =>{
        // NOTE: Usually, you would add this to the Test framework setup.
        // 
        // const sxyHelper = new TestHelper(config, this);
        // before("Authenticate", sxyHelper.authenticate);
        //
        // Now it is part of the test for demonstration purposes.

        const sxyHelper = new TestHelper(config, this);
        await sxyHelper.authenticate();
        const target = await sxyHelper.api.calculations.getAll()
        expect(target.length).to.be.greaterThan(1);
    })

});
