import { TestHelper, CalculatorLogic, SalaryKind, CalculationRowType, OAuthMessage, OAuthGrantType, AjaxNode, OAuth2, Calculations, Calculator } from "@salaxy/core";
import * as chai from "chai";

import { config } from "./config";

describe("Anonymous connection to the server and simple salary calculation.", function () {

    // Test framework setup
    const expect = chai.expect;
    const sxyHelper = new TestHelper(config, this); // Also sets the timeout

    it("Hello world: Test anonymous connection to server", () => {
        return sxyHelper.api.test.hello("Hello World") 
            .then((text) => {
                expect(text).to.be.equal("Hello World");
            });
    });

    it("A simple salary calculation without TestHelper.", () => {
        const calc = CalculatorLogic.getBlank();
        calc.rows = [
            {
                count: 1,
                price: 120,
                message: "Testing",
                rowType: CalculationRowType.Salary
            },
        ];
        const ajax = new AjaxNode();
        // Hard-coding address for demonstration purposes.
        ajax.serverAddress = "https://test-api.salaxy.com";
        return new Calculator(ajax).recalculate(calc).then((calc) => {
            expect(calc.result.totals.totalGrossSalary).to.equal(120);
        });
        ;
    });

    it("A simple salary calculation with test helper.", () => {
        const calc = CalculatorLogic.getBlank();
        
        calc.rows = [
            {
                count: 1,
                price: 120,
                message: "Testing",
                rowType: CalculationRowType.Salary
            },
        ];

        return sxyHelper.api.calculator.recalculate(calc).then((calc) => {
            expect(calc.result.totals.totalGrossSalary).to.equal(120);
        });
        ;
    });

});
