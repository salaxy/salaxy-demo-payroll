import {
    Calculation,
    CalculationRowType,
    InstantPaymentSaldo,
    Payment,
    PaymentMethod,
    UserDefinedRow,
    TaxCard,
    TaxCardType,
    TestHelper,
    WorkerLogic,
} from "@salaxy/core";
import * as chai from "chai";

import { config } from "./config";

describe("03 Create Worker, Taxcard and Calculation", function () {

    // Test framework setup
    const expect = chai.expect;
    const sxyHelper = new TestHelper(config, this);

    // Test data

    const testPerson = {
        // Change this to a non-existant Personal ID (HETU) to create a new Worker.
        // http://www.telepartikkeli.net/tunnusgeneraattori
        personalId: "240353-742H",  
        // Change the rest of the parameters just to see the effect.
        taxPercent: 23,             
        firstName: "Timo",
        lastName: "Testaaja",
        email: "timo@test.com",
        telephone: "+3581234567",
        iban: "FI4250001510000023",
    
        // This will be added by the code below.
        salaxyId: null,             
        taxcard: null,
    };
    
    // Will be created below
    let testCalculationId = null;
    let testInstantPaymentIds:string[] = [];
    let currentInstantPaymentSaldo: InstantPaymentSaldo = null;

    before("Authenticate", sxyHelper.authenticate);

    after("Delete the calculation if it was created", () => {
        if (testCalculationId) {
            return sxyHelper.api.calculations.deleteCalc(testCalculationId).then( () => {

                // Delete instant payments which were created for the calculation
                if (testInstantPaymentIds.length === 0 ) {
                    return;
                }
                const promises = testInstantPaymentIds.map((x) => {
                    return sxyHelper.api.payments.deletePayment(x);
                });
                return Promise.all(promises);
            })
        }  
    });

    it("Create a worker account.", async () => {
        const workers = await sxyHelper.api.accounts.getWorkers();
        if (workers.find((x) => x.officialPersonId == testPerson.personalId)) {
            return;
        }

        const newWorker = WorkerLogic.getBlank();
        newWorker.officialPersonId = testPerson.personalId;
        newWorker.avatar.firstName = testPerson.firstName;
        newWorker.avatar.lastName = testPerson.lastName;
        newWorker.contact.email = testPerson.email;
        newWorker.contact.telephone = testPerson.telephone;
        newWorker.ibanNumber = testPerson.iban;
        return sxyHelper.api.accounts.updateWorker(newWorker).then((newWorker) => {
            testPerson.salaxyId = newWorker.id;
            expect(testPerson.salaxyId).to.not.be.empty;
            expect(testPerson.personalId).to.equal(testPerson.personalId);
        });
    });

    it("Should get tax card based on Personal ID.", () => {
        return sxyHelper.api.taxcards.getCurrentTaxCard(testPerson.personalId).then((taxcard) => {
            if (!taxcard) {
                return;
            }
            testPerson.taxcard = taxcard;
            expect(testPerson.taxcard.id).to.not.be.empty;
        });
    });

    
    it("Should create a tax card to given Personal ID (HETU) IF not already created.", () => {
        if (testPerson.taxcard) {
            return;
        }
        const newTaxcard: TaxCard = {
            card: {
                forYear: 2018,
                date: "2018-02-01" as any,
                personalId: testPerson.personalId,
                type: TaxCardType.TaxCardA,
                taxPercent: 15,
                taxPercent2: 30,
                incomeLimit: 20000,
            }
        };
        return sxyHelper.api.taxcards.save(newTaxcard).then((storedTaxcard: TaxCard) => {
            testPerson.taxcard = storedTaxcard;
            expect(testPerson.taxcard.id).to.not.be.empty;
        });
    });

    it("Create and save a blank calculation that is then used for updating hours. ", () => {
        const calc: Calculation = {
            employer: {
                isSelf: true,
            },
            info: {
                workStartDate: "2018-03-01" as any,
                workEndDate: "2018-03-15" as any,
                workDescription: "Kokki",
            },
            worker: {
                accountId: testPerson.salaxyId,
                paymentData: {
                    // IBAN number and/or phone number are necessary ONLY if you want to override 
                    //      the ones potentially added by another employer or worker herself.
                    ibanNumber: "FI4250001510000023", 
                    telephone: "+3581234567",
                },
                tax: {
                }
            },
            salary: null,
            rows: [],
        };
        return sxyHelper.api.calculations.createOrUpdate(calc).then((resultCalc: Calculation) => {
            testCalculationId = resultCalc.id;
            expect(testCalculationId).to.not.be.empty;
            expect(resultCalc.result.totals.total).to.equal(0);
        })
        ;
    });

    it("Updating the hours until we get Update Hours running.", () => {
        expect(testCalculationId, `Calculation ID not set, probably calculation creation failed above.`).not.null;
        return sxyHelper.api.calculations.get(testCalculationId).then((calc: Calculation) => {
            expect(calc, `No calculation coming from server with ID ${testCalculationId}`).not.null;
            const weekRow: UserDefinedRow = {
                rowType: CalculationRowType.HourlySalary,
                count: 8,
                price: 15,
                message: "Päivän 2 duunit",
            };
            calc.rows.push(weekRow);
            return sxyHelper.api.calculations.createOrUpdate(calc).then((data: Calculation) => {
                expect(data.result.totals.total).to.equal(120);
            });
        });
    });

    it("Should get the calculation.", () => {
        expect(testCalculationId, `Calculation ID not set, probably calculation creation failed above.`).not.null;
        return sxyHelper.api.calculations.get(testCalculationId).then( (calc: Calculation) => {
            expect(calc, `No calculation coming from server with ID ${testCalculationId}`).not.null;
            expect(calc.result.totals.total).to.equal(120);
        });
    });

    xit("Check if there is remaining saldo for instant payment", () => {
        expect(testCalculationId, `Calculation ID not set, probably calculation creation failed above.`).not.null;
        return sxyHelper.api.payments.getInstantPaymentSaldo(testCalculationId).then( (saldo: InstantPaymentSaldo) => {
            expect(saldo, `No saldo coming from server with ID ${testCalculationId}`).not.null;
            expect(saldo.full).not.to.equal(0); 
            expect(saldo.payable).not.to.equal(0); 
            expect(saldo.methods).to.include(PaymentMethod.Sepa);
            // Set for later tests
            currentInstantPaymentSaldo = saldo;
        });
    });

    xit("Request first instant payment", () => {
        expect(testCalculationId, `Calculation ID not set, probably calculation creation failed above.`).not.null;      
        expect(currentInstantPaymentSaldo,`Current instant payment saldo not set, probably calculation creation or saldo query failed above.`).not.null;
        //Request first payment, about half
        const advance = Math.round(currentInstantPaymentSaldo.payable / 2.0);
        return sxyHelper.api.payments.createInstantPayment(testCalculationId, PaymentMethod.Sepa, advance  ).then( (payment: Payment) => {
            expect(payment, `No payment coming from server for calculation ID ${testCalculationId}`).not.null;
            testInstantPaymentIds.push(payment.id);
            expect(payment.amount).to.equal(advance);
            return sxyHelper.api.payments.getInstantPaymentSaldo(testCalculationId).then( (saldo: InstantPaymentSaldo) => {
                expect(saldo, `No saldo coming from server with ID ${testCalculationId}`).not.null;
                expect(saldo.full).not.to.equal(0); 
                expect(saldo.payable).not.to.equal(0); 
                expect(saldo.full - saldo.payable).to.equal(payment.amount)
                // Set for later tests
                currentInstantPaymentSaldo = saldo;
            });
        });
    });

    xit("Request last payment", () => {
        expect(testCalculationId, `Calculation ID not set, probably calculation creation failed above.`).not.null;      
        expect(currentInstantPaymentSaldo,`Current instant payment saldo not set, probably calculation creation or saldo query failed above.`).not.null;
        //Request last part
        const advance = currentInstantPaymentSaldo.payable; //This reduces to correct amount (remaining amount) in the server,
        return sxyHelper.api.payments.createInstantPayment(testCalculationId, PaymentMethod.Sepa, advance  ).then( (payment: Payment) => {
            expect(payment, `No payment coming from server for calculation ID ${testCalculationId}`).not.null;
            testInstantPaymentIds.push(payment.id);
            expect(payment.amount).to.equal(advance);
            return sxyHelper.api.payments.getInstantPaymentSaldo(testCalculationId).then( (saldo: InstantPaymentSaldo) => {
                expect(saldo, `No saldo coming from server with ID ${testCalculationId}`).not.null;
                expect(saldo.full).not.to.equal(0); 
                expect(saldo.payable).to.equal(0); 
            });
        });
    });
});
