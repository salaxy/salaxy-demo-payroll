import { TestHelper, CalculatorLogic, SalaryKind, CalculationRowType, OAuthMessage, OAuthGrantType, AjaxNode, OAuth2, Calculations, Test, Ajax, WorkerAccount, TaxCardType, TaxCardState, TaxCardIncomeType, Calculation } from "@salaxy/core";
import * as chai from "chai";

import { config } from "./config";

describe("04 Demonstrates for a trusted partner, how to create salary payments in Salaxy.", function () {

    // Test framework setup
    const expect = chai.expect;
    const sxyHelper = new TestHelper(config, this);

    // TODO: Before starting, addd your own ID's here
    const personalId = "280887-485L";
    const salaxyAccountId = "XXXX";

    before("Authenticate", sxyHelper.authenticate);

    xit("Hello world: Test anonymous connection to server, no test-helper.", async () => {
        const ajax = new AjaxNode();
        // For production, you would add this.
        // ajax.serverAddress = "https://secure.salaxy.com";
        const testApi = new Test(ajax);
        const text = await testApi.hello("Hello World");
        expect(text).to.be.equal("Hello World");
    });

    xit("Should authenticate to server without test helper.", async () => {
        const request: OAuthMessage = {
            grant_type: OAuthGrantType.Password,
            username: config.username,
            password: config.password,
        };
        const ajax = new AjaxNode();
        ajax.serverAddress = "https://test-api.salaxy.com";
        const calcs = await new OAuth2(ajax).token(request).then((oauthResponse) => {
            ajax.setCurrentToken(oauthResponse.access_token);
            return oauthResponse;
        })
            .then((oauthmsg) => {
                return new Calculations(ajax).getAll();
            });
        expect(calcs.length).to.be.greaterThan(0);
    })

    xit("should create a Worker in a way that the ID does not change.", async () => {
        var workerAccount = await assureWorkerAccount(sxyHelper.ajax, personalId, 
            "Teppo", "Tester", "teppo@palkkaus.fi", "123 45586", "NL15RABO0153089356");
        expect(workerAccount.id).to.not.be.null;
    });

    xit("Should get tax card based on Personal ID, if none exists, create a new one.", async () => {
        const existing = await sxyHelper.api.taxcards.getCurrentTaxCard(personalId);
        if (!existing) {
            sxyHelper.api.taxcards.save({
                card: {
                    type: TaxCardType.TaxCardA,
                    forYear: 2018,
                    date: "2018-02-01",
                    taxPercent: 20,
                    incomeLimit: 30000,
                    taxPercent2: 40,
                    personalId: personalId,
                }
            });
        }
    });

    xit("Create and save a blank calculation that is then used for updating hours. ", async () => {
        const calc: Calculation = {
            employer: {
                isSelf: true,
            },
            info: {
                workStartDate: "2018-09-01" as any,
                workEndDate: "2018-09-15" as any,
                workDescription: "Toimistotyöntekijä",
                occupationCode: "12345",
            },
            worker: {
                accountId: salaxyAccountId,
                paymentData: {
                    // IBAN number and/or phone number are necessary ONLY if you want to override 
                    //      the ones potentially added by another employer or worker herself.
                    ibanNumber: "FI4250001510000023", 
                    telephone: "+3581234567",
                },
            },
            framework: {
                numberOfDays: 10,
            },
            usecase: {
                uri: "palkkaus.fi/testing",
            },
            workflow: {

            },
            salary: null,
            rows: [
                {
                    count: 10,
                    price: 15,
                    rowType: CalculationRowType.HourlySalary,
                    message: "L&T company work", 
                }
            ],
        };
        var result = await sxyHelper.api.calculations.createOrUpdate(calc);
        expect(result).to.not.be.empty;
        expect(result.result.totals.total).to.equal(150);
    });

});

/** 
    * Assures that there is a Worker account to which Salaxy can pay salaries.
    * If account has already been created for the Social security number (Henkilötunnus / HETU in Finnish), returns the basic information.
    * If the account does not exist, an account stub is created based on the Social security number.
    *
    * @param ajax - Salaxy Ajax object that handles the HTTP connection to the server.
    * @param officialId - Social security number for the person: Henkilötunnus / HETU in Finnish
    * @param firstName - First name of the person
    * @param lastName - Last name of the person
    * @param email - Contact e-mail
    * @param telephone - Telephone number for the contact person
    * @param bankAccountIban - Optional bank account IBAN number
    * returns WorkerAccount
    */
function assureWorkerAccount(ajax: Ajax, officialId: string, firstName: string, lastName: string, email: string, telephone: string, bankAccountIban: string): Promise<WorkerAccount> {
    var method = "/partner/assureworker";

    method += "?officialId=" + encodeURIComponent(officialId || "") +
        "&firstName=" + encodeURIComponent(firstName || "") +
        "&lastName=" + encodeURIComponent(lastName || "") +
        "&email=" + encodeURIComponent(email || "") +
        "&telephone=" + encodeURIComponent(telephone || "") +
        "&bankAccountIban=" + encodeURIComponent(bankAccountIban || "");
    return ajax.postJSON(method, null);
};
