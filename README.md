DEMO: Integrating internal Payroll to Salaxy
============================================

This is Demonstration on how to integrate your own payroll systems to Salaxy interface.
The idea is that you have hourly reporting systems, payroll lists etc.
from which you want to create objects in to the Salaxy database, e.g.:

- Employees (Workers in Salaxy terminology)
- Taxcards for Widthholding tax
- Employment Contracts
- Salary Calculations
- Payments
- Reports

The demo site is still quite simple, but we are adding new functionality.
**If you are interested in a particular scenario, please ask!**
We would be happy to prioritize your scenario.

All demos are currently modeled as Chai/Mocha tests, but the TypeScript / JavaScript 
code is universal: You can use it any kind of JavaScript environment.

Getting started with the tests:
-------------------------------

1. Clone repository `git clone https://gitlab.com/salaxy/salaxy-demo-payroll`
2. Enter project directory `cd salaxy-demo-payroll`
3. Run `npm install` for installing required modules
4. Copy `tests/config-template.txt` as `config.ts` and set your own UID / PWD.
  - You may create a UID/PWD in https://test.palkkaus.fi. Use e-mail authentication, not Google or Facebook.
5. Run the tests `npm run test`
  - For a single test file e.g. `npm run single-test tests/basic-integration.spec.ts` 
6. See the source code to understand how the operations are being done

To learn more:
--------------

- Install VisuaStudio Code 
  - **SERIOUSLY:** with TypeScript **intellisense** it is by far the best tool for learning this kind of code
- If you edit tests, install extensions:
  - TSLint
  - Mocha sidebar
- Go to https://developers.salaxy.com
  - API reference: https://developers.salaxy.com/#/api/swagger
  - Business objects browser: https://developers.salaxy.com/#/api/models (only for large screens)
  - API JavaScript wrappers: https://developers.salaxy.com/jquery.html#/docs/core/module/salaxy.api
  - Model JavaScript documentation: https://developers.salaxy.com/jquery.html#/docs/core/module/salaxy.api
    - ...though VSCode intellisense and API reference & BO-browser are typically better tools for understanding the model

Getting started from scratch
============================

To create such a site from scratch or to add the functionality to your own framework:

- If you do not have TypeScript installed: `npm install -g typescript`
  - NOTE: This is not a requirement for `@salaxy/core` library.
    You could use plain JavaScript instead. Only our demos are in TypeScript
- Install Salaxy core libraries `npm install @salaxy/core --save`
- To run @salaxy/core in Node, you also need:
  - `npm install request --save-dev`
  - `npm install request-promise --save-dev`
- The rest of depenencies in package.json are for Mocha/Chai and its intellisense.
  You need the only if you are using Mocha/Chai testing framework.

Full set of dependencies in `package.json`:

```
  "devDependencies": {
    "request": "^2.87.0",
    "request-promise": "^4.2.2",
    "@types/chai": "^4.1.0",
    "@types/mocha": "^2.2.46",
    "@types/request": "^2.0.9",
    "@types/request-promise": "^4.1.39",
    "chai": "^4.1.2",
    "dirty-chai": "^2.0.1",
    "mocha": "^4.1.0",
    "ts-node": "^4.1.0"
  },
  "dependencies": {
    "@salaxy/core": "^2.0.20"
  }
```
